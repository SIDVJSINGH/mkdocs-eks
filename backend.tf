terraform {
  backend "s3" {
    bucket = "834277767436-tf-state"
    key    = "EKS/eks.tfstate"
    region = "us-east-1"
  }
} 